/** @type {import('next').NextConfig} */
const nextConfig = {
    env: {
        apiBaseUrl: 'http://localhost:5179'
    }
}

module.exports = nextConfig
