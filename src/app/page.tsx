import { Button } from "@mui/material";
import styles from "./page.module.css";

export default function Home() {
  return (
    <main className={styles.main}>
      <div className={styles.container}>
        <h1>Click button below to check current exchange rates</h1>
        <Button variant="outlined" href="/exchangerates">
          C&apos;mon and check!
        </Button>
      </div>
    </main>
  );
}
