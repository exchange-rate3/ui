import Link from "next/link";
import styles from "./not-found.module.css";

export default function NotFound() {
  return (
    <main className={styles.main}>
      <div className={styles.container}>
        <h1>Page not found :(</h1>
        <Link href="/">Return Home</Link>
      </div>
    </main>
  );
}
