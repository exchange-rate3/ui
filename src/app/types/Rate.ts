export type Rate = {
  code: string;
  currency: string;
  mid: number;
  effectiveDate: string;
};
