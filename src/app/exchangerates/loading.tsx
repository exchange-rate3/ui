"use client";
import { Grid, Skeleton, Typography } from "@mui/material";
import styles from "./loading.module.css";

export default function Loading() {
  return (
    <main className={styles.main}>
      <div>
        <Typography variant="h3" gutterBottom>
          <Skeleton height="100%" />
        </Typography>
        <Grid container flexDirection={"column"} rowSpacing={1}>
          {[...Array(10)].map((_, idx) => (
            <Grid key={idx} item md>
              <Skeleton variant="rectangular" width="100%" height={50} />
            </Grid>
          ))}
        </Grid>
      </div>
    </main>
  );
}
