import CurrencyDataGrid from "@components/CurrencyDataGrid";
import { Typography } from "@mui/material";
import Link from "next/link";
import { Rate } from "../types/Rate";
import styles from "./page.module.css";

const getCurrencies = async (): Promise<Rate[]> => {
  const res = await fetch(`${process.env.apiBaseUrl}/ExchangeRates`, {
    next: {
      revalidate: 60,
    },
  });

  return (await res.json()) as Rate[];
};

const getLastSynchronizationDate = async (): Promise<string> => {
  const response = await fetch(
    `${process.env.apiBaseUrl}/ExchangeRates/LastSynchronizationDate`,
    { cache: "no-cache" }
  );

  const date = await response.text();

  return new Date(date.replaceAll('"', "")).toLocaleString();
};

export default async function Home() {
  const [data, lastSynchronizationDate] = await Promise.all([
    getCurrencies(),
    getLastSynchronizationDate(),
  ]);

  return (
    <main className={styles.main}>
      <div className={styles.container}>
        <Typography variant="h3" gutterBottom>
          Welcome to Exchange Rate
        </Typography>
        <CurrencyDataGrid data={data} />
        <Typography variant="caption" marginTop={5}>
          Time of last synchronization: {lastSynchronizationDate}
        </Typography>
        <br />
        <Link href="/">Return Home</Link>
      </div>
    </main>
  );
}
