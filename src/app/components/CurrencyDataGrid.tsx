"use client";

import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";
import { Rate } from "../types/Rate";

type RateColDef = GridColDef & { field: keyof Rate };

const columns: RateColDef[] = [
  {
    field: "currency",
    headerName: "Currency",
    minWidth: 150,
    flex: 2,
  },
  {
    field: "code",
    headerName: "Code (symbol)",
    minWidth: 150,
    flex: 1,
    valueGetter: (params: GridValueGetterParams) =>
      `${params.row.code || ""} (${
        new Intl.NumberFormat("en", {
          style: "currency",
          currency: params.row.code,
        })
          .formatToParts(1)
          .find((x) => x.type === "currency")?.value
      })`,
  },
  {
    field: "mid",
    headerName: "Rate (PLN)",
    description: "Average exchange rate based on previous day.",
    minWidth: 100,
    flex: 1,
  },
  {
    field: "effectiveDate",
    headerName: "Published date",
    description: "Date when NBP published mid exchange rate.",
    minWidth: 160,
    flex: 1,
  },
];

interface ICurrencyDataGridProps {
  data: Rate[];
}

const CurrencyDataGrid = ({ data }: ICurrencyDataGridProps) => {
  return (
    <DataGrid
      style={{ width: "100%" }}
      rows={data}
      columns={columns}
      rowSelection={false}
      initialState={{
        pagination: {
          paginationModel: { page: 0, pageSize: 20 },
        },
      }}
      getRowId={(row) => row.code}
      pageSizeOptions={[5, 10, 20, 50, 100]}
    />
  );
};

export default CurrencyDataGrid;
